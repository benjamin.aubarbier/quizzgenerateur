<?php


$i=1;                                                                                                    //Initialisation de la variable $i
$quizz=file('donnee.txt');                                                                                //Création du tableau quizz, chaque case correspond à une ligne de question/rep


foreach($quizz as $valeur)                                                                               //Boucle qui a deux foctions :
{                               
${"tab".$i} = explode ('##',$valeur);                                                                    //1 : découpage de chaque ligne avec les dièses



${"ans".$i}= substr($valeur, ($par = strpos($valeur, '(')+1), strrpos($valeur, ')')-$par);              //2 : découpage de la réponse correspondante à une ligne


$i++;                                                                                                   //Incrémentation
$score =0;
}

$count = count($quizz);
$nbr=count($_POST);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Reponses</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="result.css" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <script src="main.js"></script>
</head>  
    
    <?php 
    
    $doc = fopen("resultats.txt", "w+");
    for ($i=1; $i<=$nbr; $i++)
    {
        
        fwrite($doc, "\n Réponse".$i." : ".$_POST[$i]."  alors que la bonne réponse est ".${"ans".$i}."\r" );
        fwrite($doc, "\n");
        if ($_POST[$i] == ${"ans".$i})
        {
            fwrite($doc, "Bonne réponse");
            fwrite($doc, "\n");
            $score=$score+1;
        }
        else
        {
            fwrite($doc, "Mauvaise Réponse");
            fwrite($doc, "\n");
        }
        
    }
    $prc = ($score*100)/$count;
    $prc = round($prc,0);
    $fail = $count-$score;
    fwrite($doc,"---------------Votre score est de : ".$score." sur ".$count." questions---------------");
    if ($prc >= 75)
    {
        $img = "images/good.png";
        $txt ="Félicitations !!";
    }
    if ($prc > 50 && $prc < 75)
    {
        $img = "images/medium.png";
        $txt ="Bien joué !";
    }
    if ($prc <= 50 )
    {
        $img = "images/bad.png";
        $txt ="Tu feras mieux la prochaine fois !";
    }
    
    ?>
    <body>
    <h1 class="title">Ton score est de <?php echo $prc; ?>%</h1>
    <section class="main"> 

    <div>
    <div class="good"><p class="number"><?php echo$score; ?></p></div>
    <p>Bonnes réponses</p>
    </div>


    <div>
    <img src="<?php echo $img ?>">
    <h3 class="title"><?php echo $txt ?></h3>
    </div>


    <div>
    <div class="bad"><p class="number"><?php echo$fail; ?></p></div>
    <p >Mauvaises réponses</p>
    </div>
    </section>
    </body>


</html>

