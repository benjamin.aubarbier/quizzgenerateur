<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Quiz HTML</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="homepage.css" />
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

    <script src="main.js"></script>
</head>



<body>
<header >
<img class="main_title" src="images/logo.png">
</header>
    <div class="section_one">

        <img class="icone_one" src="./image/imageone.png">

        <div class="text_one">
            <h3 class="title_one">Prenez le pouls <span class="title_one">de votre niveau !</span></h3>
            <p class="subtitle_one">Pensez vous tous savoir en HTML / CSS ?</p> <p class="subtitle_one_two">Ce quiz est accessible a tout type de niveau !</p>
        </div>

    </div>

    <div class="section_two">

        <img class="icone_two" src="./image/imagetwo.png">

        <div class="text_two">
            <h3 class="title_two">Améliorez <span class="title_two"> vos connaissance !</span></h3>
            <p class="subtitle_two">Enfin, un quiz permettant de progresser en HTML / CSS</p> <p class="subtitle_two_two">Et de se préparer pour un éventuel partiel ! </p>
        </div>

    </div>
<a class="lien" href="main.php">
    <div class="commencer">
    <div class="commencer_two">
        <p class="text_commencer">Commencer</p>
        <img class="icone_commencer" src="./image/imagefive.png">
    </div>
    </div>
    </a>    
</body>

</html>